#include<iostream>
#include<limits.h>  
#include<string>

using namespace std; 

#define vertices 11

// se busca el vertice con la distancia minima.
int pesominimo(int peso[], bool visitado[]){

    int minimo = INT_MAX;
    int indice = 0;

    for (int i = 0; i < vertices; i++){
        if (visitado[i] == false && peso[i] < minimo){

            minimo = peso[i];
            indice = i;
        }
    }

    return indice;
}

void dijkstra(int grafo[11][11], int raiz, string *LN){ //#int numero_vertices
    
    // int array para calcular la distancia minima en cada nodo 
    int peso[vertices];
    // boolean array para marcar los nodos visitados y no visitados.
	bool visitado[vertices];

    //marcar que todos los nodos tengan el peso maximo / infinito
    fill(peso, peso+vertices, INT_MAX ) ; 
    //marcar todos los nodos como no visitados
    fill(visitado, visitado+vertices, false);

    // marcar el nodo raiz con la distancia actual de cero
    peso[raiz] = 0;  

    for(int i = 0; i<vertices; i++)                           
	{
        // peso minimo devuelve el indice del vertice que pesa menos
		int minimo=pesominimo(peso,visitado); 
        //marcamos el nodo actual como visitado 
		visitado[minimo]=true;
        
		for(int i = 0; i< vertices; i++)                  
		{
			// cambiar el peso minimo por el nuevo peso
			if(visitado[i] == false && grafo[minimo][i] &&
              peso[minimo] != INT_MAX && (peso[minimo]+grafo[minimo][i]) < peso[i])

				peso[i]=peso[minimo]+grafo[minimo][i];
		}
	}
    // mostrar el nodo  y el peso minimo desde la raiz
	cout<<"Vertice\t\tPeso minimo desde la raiz"<<endl;
    
	for(int i = 0; i<vertices; i++)                      
	{ 
		int numero_vertices = i;
       

		cout<< LN[i]<<"\t\t"<<peso[i]<<endl;
        if(peso[i] == INT_MAX)
            cout<<"\t\tINFINITO" << endl;
        cout<< ".........................."<< endl;
	}

}

int main(){

    string Lista_PROTEINAS[11] ={"TFG","ALK","EML4","TPM3","NTRK1","TPR",
                                "SEC13","SEC31A","SEC16A","SEC24C","SEC24B"};
    int grafo[11][11]={
        
		{0, 13, 13, 11, 11, 12, 13, 12, 10, 12, 10},
		{13, 0, 10, 11, 0, 0, 0, 23, 0, 0, 0},
		{11, 10, 0, 16, 18, 0, 0, 0, 0, 0, 0},
		{11, 11, 16, 0, 11, 12, 0, 21, 0, 0, 0},
		{11, 0, 12, 11, 0, 11, 0, 0, 0, 0, 0},
		{12, 0, 0, 12, 11, 0, 12, 0, 0, 0, 0},
        {13, 0, 0, 0, 0, 12, 0, 10, 10, 10, 10},
        {12, 0, 0, 21, 0, 0, 10, 0, 10, 10, 10},
        {10, 0, 0, 0, 0, 0, 0, 10, 0, 10, 0},
        {12, 0, 0, 0, 0, 0, 10, 10, 10, 10, 10},
        {13, 0, 0, 0, 0, 0, 10, 10, 10, 10, 0}
        };

   dijkstra(grafo,0, Lista_PROTEINAS);
    return 0;
}
