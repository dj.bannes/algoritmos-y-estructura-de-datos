#include<iostream>
#include<string>

using namespace std;

class Estudiante{
    private: 
        string nombre;
        int edad;

    public:
        Estudiante(string nombre, int edad){
            this->nombre = nombre;
            this->edad = edad;
        }

        Estudiante(){
            this->nombre = "";
            this->edad = 0;
        }

        string getNombre(){
            return nombre;
        }

        void setNombre(string nombre){
            this->nombre = nombre;
        }   

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};


//Por comodidad, es mejor definir la clase "Nodo" para luego definir la lista
//(cada nodo esta formado por los datos y por su "apuntador" al siguiente)
class NodoEstudiante{
    private:
        Estudiante valor;
        NodoEstudiante *vecino;

    public:
        NodoEstudiante(){
            this->vecino = NULL;
        }

        NodoEstudiante(Estudiante nuevo){
            this->valor.setNombre(nuevo.getNombre());
            this->valor.setEdad(nuevo.getEdad());
            this->vecino = NULL;
        }

        void setVecino(NodoEstudiante *vec){
            this->vecino = vec;
        }
                
        NodoEstudiante *getVecino(){
            return(this->vecino);
        }

        Estudiante *getEstudiante(){
            return(&this->valor);
        }

};

class ListaEstudiantes{
    private:
        NodoEstudiante *primero, *ultimo;

    public:
        ListaEstudiantes(){
            primero = ultimo = NULL;

        }

        void insertarNodo(NodoEstudiante *nuevo){
            if (ultimo == NULL){
                primero = ultimo = nuevo;
            }else{
                ultimo->setVecino(nuevo);
                ultimo = nuevo;
            }            
        }

        void mostrarLista(){
            NodoEstudiante *recorrer;

            recorrer = primero;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                cout <<"Estudiante" << endl;
                cout <<"-- Nombre: " << estudiante->getNombre()  << endl;;
                cout <<"-- Edad: " << estudiante->getEdad() << endl;

                recorrer = recorrer->getVecino();
           }
        }
        void buscarEstudiante(string nombre){ 
            NodoEstudiante *recorrer;

            recorrer = primero;
            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();
                cout<< estudiante->getNombre()<<endl;
                if(estudiante->getNombre() == nombre){

                    cout<<boolalpha<<"Nombre: "<<true<<endl;
                    //boolalpha para entregar la palabra true y no un numero,
                    //que en el caso de true es 1.(lo mismo para false)                    
                }else{
                    cout<<boolalpha<<"Nombre: "<<false<<endl;
                    //en el caso de false el booleano int = 0

                }      
                
                recorrer = recorrer->getVecino();
           }      

        }

        void insertarPrimero(NodoEstudiante *nuevo){//insertar primero 
            nuevo->setVecino(primero);
            primero = nuevo;           
        }

        void quitarPrimero(){
            NodoEstudiante *nuevo;

            nuevo = primero;
            primero = nuevo->getVecino();
   
        }
        void quitarUltimo(){
            NodoEstudiante *nuevo,*nuevo2;//nuevo2 sera igual a nuevo

            nuevo = primero;
            if (primero->getVecino()== NULL){
                primero = NULL;
            
            }else{
                while (nuevo->getVecino()!= NULL){
                    nuevo2 = nuevo;
                    nuevo = nuevo->getVecino();    
                }
                
                nuevo2->setVecino(NULL);//se borra el ultimo 
                
            }
        }

};

int main(){    
    ListaEstudiantes lista;
    Estudiante a("Estudiante 1", 20);
    NodoEstudiante *nuevo= new NodoEstudiante(a);
    lista.insertarNodo(nuevo);

    Estudiante b("Estudiante 2", 30);
    NodoEstudiante *nuevo2= new NodoEstudiante(b);
    lista.insertarNodo(nuevo2);

    Estudiante c("Estudiante 3", 30);
    NodoEstudiante *nuevo3= new NodoEstudiante(c);
    
    int opcion;
    string opcion2;   
    bool repetir = true;

    do{

        
        cout<<"\nMenu de opciones"<<endl;
        cout<<"\t1 buscar estudiante"<<endl;
        cout<<"\t2 insertarPrimero"<<endl;
        cout<<"\t3 quitarUltimo"<<endl;
        cout<<"\t4 quitarPrimero"<<endl;
        cout<<"\t0 Salir"<<endl;

        cout<<"\n\t ingrese una opcion";
        cin >> opcion;

        switch (opcion){

        case 1:
            lista.mostrarLista();
            cout<<"\n\t ingrese un nombre de un estudiante\t";
            cin.ignore();
            getline(cin,opcion2);
            lista.buscarEstudiante(opcion2);

            break;
        case 2:
            
            lista.insertarPrimero(nuevo3);
            lista.mostrarLista();

            break;
        case 3:
            
            lista.quitarUltimo();
            lista.mostrarLista();

            break;
        case 4:
            
            lista.quitarPrimero();
            lista.mostrarLista();

            break;
        case 0:
            repetir = false;
            break;
        
        default:
            cout<<"la opcion que ingreso es incorrecta"<<endl;
            break;
        }

    }while(repetir);
    

  
    
    return 0;
}
