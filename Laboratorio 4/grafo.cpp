#include<list>
#include "grafo.h"
using namespace std;

Tipografo::Tipografo(int maxV) {
    numvertices = 0;
    maxvertices = maxV;
    vertices = new int[maxV];
    aristas = new int * [maxV];
    adjLists = new list<int>[maxV];

    for (int i = 0; i < maxV; i++)
        aristas[i] = new int[maxV];

    marks = new bool[maxV];

}

Tipografo::~Tipografo() {
    delete[] vertices;

    for (int i = 0; i < maxvertices; i++)
        delete[] aristas[i];

    delete[] aristas;
    delete[] marks;
}

void Tipografo::anadir_vertice(int vertex) {
    vertices[numvertices] = vertex;

    for (int index = 0; index < numvertices; index++) {
        aristas[numvertices][index] = NULL_EDGE;
        aristas[index][numvertices] = NULL_EDGE;
    }

    numvertices++;
}

void Tipografo::anadir_arista(int desdeVertex, int haciaVertex, int peso) {
    int fila, colum;
    cout << "Ingreso a Añadir Arista" << endl;
    adjLists[desdeVertex].push_front(haciaVertex);//dato ingresado al principio de la lista 
    cout << "Paso la lista de adyacencia " << endl;
    fila = indice(vertices, desdeVertex);
    colum = indice(vertices, haciaVertex);
    aristas[fila][colum] = peso;
}

int Tipografo::Peso(int desdeVertex, int haciaVertex) {
    int fila, colum;
    fila = indice(vertices, desdeVertex);
    colum = indice(vertices, haciaVertex);

    return aristas[fila][colum];
}

int Tipografo::indice(int * vertices, int vertex) {
    for (int i = 0; i < numvertices; i++) {
        if (vertices[i] == vertex)
            return i;
    }

    return -1;
}

bool Tipografo::estalleno() {
    if (numvertices == maxvertices)
        return true;
        
    return false;
}

bool Tipografo::estavacio() {
    if (numvertices == 0){
        cout<<"Vacio"<< endl;
        return true;
    }
        
        
    cout<<"Ocupado"<< endl;
    return false;
}

int Tipografo::tamanografo() {
    return numvertices;
}

void Tipografo::DFS(int vertex) {
    marks[vertex] = true;
    cout << "Ingreso a la Busqueda avanzada" << endl;
    list<int> adjList = adjLists[vertex];
   

    cout << vertex << "---";
    cout << "Valor del vertice" << endl;
    list<int>:: iterator i;
    for (i = adjList.begin(); i != adjList.end(); i++)
        if (!marks[*i])
            DFS(*i);
}
