#include <iostream> 
#include "grafo.h" 

using namespace std; 

int main() { 
    Tipografo x(100); 

    cout << "\nestado del vertice " << x.estavacio()<< "\n\ncantidad de vertices : " << x.tamanografo() << endl;
    /*El grafo es unidireccional*/
    
    x.anadir_vertice(10);
    x.anadir_vertice(20);
    cout << "\nestado del vertice : " << x.estavacio() << "\n\ncantidad de vertices : " << x.tamanografo() << endl;
    x.anadir_arista(10, 20, 580);
    cout << "Peso: 10 -> 20 : " << x.Peso(10, 20) << endl;
    x.anadir_arista(20, 10, 580);
    cout << "Peso: 20 -> 10 : " << x.Peso(20, 10) << endl;
    x.anadir_vertice(30);
    cout << "\nestado del vertice : " << x.estavacio() << "\ncantidad de vertices : " << x.tamanografo() << endl;
    x.anadir_arista(10, 30, 200);
    cout << "Peso: 10 -> 30 : " << x.Peso(10, 30) << endl;
    x.anadir_arista(30, 10, 200);
    cout << "Peso: 30 -> 10 : " << x.Peso(30, 10) << endl;
    x.anadir_vertice(40);
    cout << "\nestado del vertice : " << x.estavacio() << "\n\ncantidad de vertices : " << x.tamanografo() << endl;
    x.anadir_arista(30, 40, 250);
    cout << "Peso: 30 -> 40 : " << x.Peso(30, 40) << endl;
    x.anadir_arista(40, 30, 250);
    cout << "Peso: 40 -> 30 : " << x.Peso(40, 30) << endl;
    x.anadir_vertice(50);
    cout << "\nestado del vertice : " << x.estavacio() << "\n\ncantidad de vertices : " << x.tamanografo() << endl;
    x.anadir_arista(40,50, 300);
    cout << "Peso: 40 -> 50 : " << x.Peso(40, 50) << endl;
    x.anadir_arista(50,40, 300);
    cout << "Peso: 50 -> 40 : " << x.Peso(50, 40) << endl;

    cout << "selecciona el vertice desde donde comienza la busqueda en profundidad : " << endl;
    int node;
    cin >> node;
    // se selecciona el vertice desde donde comienza la busqueda en profundidad
    x.DFS(node);
    
   
} 