#include<iostream>
#include<list>
using namespace std;


const int NULL_EDGE = 0;
class Tipografo {
    private:
      int numvertices;
      int maxvertices;

      int* vertices;
      int **aristas;
      list<int> *adjLists;
      bool* marks;
    public:
      Tipografo(int);
      ~Tipografo();
      void anadir_vertice(int);
      void anadir_arista(int, int, int);
      int Peso(int, int);
      int indice(int*,int);
      bool estavacio();
      bool estalleno();
      int tamanografo();
      void DFS(int);
    

};